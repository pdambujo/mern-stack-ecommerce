import mongoose from 'mongoose';
import db from '../config/db';

mongoose.set('strictQuery', false);
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/e_commerce_db', {
  useNewUrlParser: true,
});

const connection = mongoose.connection;

connection.once('open', () => {
  console.log('MongoDB database connection established successfully!');
});

export default connection;
