import { sign } from 'jsonwebtoken';

const generateToken = (id) => {
  return sign({ id }, process.env.JWT_SECRET_KEY, { expiresIn: '3d' });
};

export default generateToken;
