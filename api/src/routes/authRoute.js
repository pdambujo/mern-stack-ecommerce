import express from 'express';
import {
  createUser,
  deleSingleUser,
  getAllUser,
  getSingleUser,
  loginCtrl,
  updateUser,
} from '../controllers/userController';

const router = express.Router();

router.post('/register', createUser);
router.post('/login', loginCtrl);
router.get('/all-users', getAllUser);
router.get('/:id', getSingleUser);
router.delete('/:id', deleSingleUser);
router.put('/:id', updateUser);

export default router;
