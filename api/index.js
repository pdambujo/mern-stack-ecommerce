import express from 'express';
import dotenv from 'dotenv';
import connection from './src/config/db';
import authRouter from './src/routes/authRoute.js';
import bodyParser from 'body-parser';
import { notFound, errorHandling } from './src/middlewares/errorHandling';

dotenv.config();

const app = express();
const PORT = process.env.PORT || 4000;
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());
// Routes
app.use('/api/v1/user', authRouter);

// error handling
app.use(notFound);
app.use(errorHandling);

app.listen(PORT, () => {
  console.log(`Backend API is running on ${PORT}`);
});
